#include "poly3.h"

#include <gtsam/base/Vector.h>

#include <Eigen/Dense>

using namespace gtsam;
using namespace std;

/* ************************************************************************* */
Poly3::Poly3 (const Node &node1, const Node &node2) {
  Matrix4 A;
  Vector4 b;
  double x1 = node1(0), x2 = node2(0);
  A << 1, x1, x1*x1, x1*x1*x1,
      1, x2, x2*x2, x2*x2*x2,
      0, 1,  2*x1,  3*x1*x1,
      0, 1,  2*x2,  3*x2*x2;
  b << node1(1), node2(1), node1(2), node2(2);
  data = A.colPivHouseholderQr().solve(b);
}

/* ************************************************************************* */
void Poly3::print(const string &s) const {
    cout << s << "(" << data(0) << "," << data(1) << "," << data(2) << ","
         << data(3) << ")" << endl;
}

/* ************************************************************************* */
bool Poly3::equals(const Poly3 &poly3, double tol) const {
    return equal_with_abs_tol(data, poly3.data, tol);
}

/* ************************************************************************* */
Poly3 Poly3::retract(const Vector4 &v) const {
    return Poly3(data+v);
}

/* ************************************************************************* */
Vector4 Poly3::localCoordinates(const Poly3 &s) const {
    return s.data - data;
}

/* ************************************************************************* */
double Poly3::y(const double& x, gtsam::OptionalJacobian<1, 4> J_data,
                gtsam::OptionalJacobian<1, 1> J_x) const {
    double y = data(0) + data(1) * x + data(2) * x*x + data(3) * x*x*x;

    if (J_data) {
        *J_data << 1, x, x*x, x*x*x;
    }

    if (J_x) {
        *J_x << data(1) + data(2) * 2*x + data(3) *3*x*x;
    }

    return y;
}

/* ************************************************************************* */
Poly3 Poly3::Create(const Node& node1, const Node& node2) {
  return Poly3(node1, node2);
}
