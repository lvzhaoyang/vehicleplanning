
#include "poly3.h"
#include "poly1.h"

#include <vector>
#include <memory>

/**
 * The Spline a vector
 */
template<typename Node, typename Poly>
class Spline {

public:
  typedef std::vector<Node> Nodes;
  typedef std::vector<Poly> Polys;
  typedef std::shared_ptr<Nodes> Nodes_ptr;
  typedef std::shared_ptr<Polys> Polys_ptr;

  ///@name Constructors
  ///@{

  Spline() {
    nodes = std::make_shared<Nodes>();
    polys = std::make_shared<Polys>();
  }

  ///@}

  void setup (const Nodes_ptr& new_nodes) {
  }

private:
  Nodes_ptr nodes; ///< the nodes of all splines
  Polys_ptr polys;   ///< the polynomials between the nodes

};

/// a trajectory is a spline
class Traj {

public:

  typedef Spline<Vector3, Poly3> GeometrySpline;
  typedef Spline<double, Poly1> SpeedSpline;

  /// @name Constructors
  /// @{

  Traj() {
    geometry_spline = std::make_shared<GeometrySpline>();
    speed_spline = std::make_shared<SpeedSpline>();
  }

private:

  std::shared_ptr<GeometrySpline> geometry_spline;
  std::shared_ptr<SpeedSpline> speed_spline;

  double prob;    /// the probability of the trajectory


};


