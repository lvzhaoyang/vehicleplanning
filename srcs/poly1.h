
#pragma once

#include <gtsam/base/Manifold.h>
#include <gtsam/base/Matrix.h>
#include <gtsam/nonlinear/Expression.h>

typedef gtsam::Vector2 Vector2;

/** A polynomial of order 1: a linear function
 */
class Poly1 {

private:
    Vector2 data; ///< the coefficents of the polynomial

public:

    enum {
      dimension = 2
    };

    /// @name constructors
    /// @{
    Poly1() {}

    Poly1(double a0, double a1) {
        data = Vector2(a0, a1);
    }

    explicit Poly1(const Vector2& poly)
        : data(poly) {
    }

    /// @}
    /// @name Testable
    /// @{

    /** print with optional string*/
    void print(const std::string& s="") const;

    bool equals(const Poly1& poly1, double tol=1e-9) const;

    /// @{
    /// @name Manifold
    /// @{

    /** retract function*/
    Poly1 retract(const Vector2& v) const;

    /** local coordinate function*/
    Vector2 localCoordinates(const Poly1& s) const;

    /** Dimensionality of the tangent space = 4DOF*/
    inline static size_t Dim() {
        return dimension;
    }

    /** Dimensionality of the tangent space = 4DOF*/
    inline size_t dim() const {
        return dimension;
    }

    /// @}
    /// @name evaluations
    /// @{

    /** return the coefficients of the data*/
    Vector2 coefficients() const {
        return data;
    }

    /** return the Y coordinate value, given a X
     * @param x
     * @param J_data Jacobian w.r.t. data
     * @param J_x Jacobian w.r.t. x
     */
    double y (const double& x,
              gtsam::OptionalJacobian<1,2> J_data = boost::none,
              gtsam::OptionalJacobian<1,1> J_x = boost::none) const;

    /** Create a Poly3 type given two nodes
     * @param node1
     * @param node2
     * @return
     */
//    static Poly1 Create(const Node& node1, const Node& node2);
};

namespace gtsam {
  template<> struct traits<Poly1> : public internal::Manifold<Poly1> {
  };

  template<> struct traits<const Poly1> : public internal::Manifold<Poly1> {
  };

  /// Define expression for gtsam

typedef Expression<Poly1> Poly1_;
typedef Expression<double> double_;

static double_ Evaluate_Y(const Poly1_& poly1, const double_& x) {
  return double_(poly1, &Poly1::y, x);
}

}

