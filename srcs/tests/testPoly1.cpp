#include "../poly1.h"

#include <gtsam/base/numericalDerivative.h>

#include <CppUnitLite/TestHarness.h>

using namespace std;
using namespace gtsam;

GTSAM_CONCEPT_TESTABLE_INST(Poly1)
GTSAM_CONCEPT_MANIFOLD_INST(Poly1)

/* ************************************************************************* */
TEST(Poly1, constructor) {
  {
    Vector2 data;
    data << 1.0, 2.0;

    Poly1 poly(data);
    EXPECT(assert_equal(data, poly.coefficients(), 1e-8));
  }
  {
    Vector2 data;
    data << 1.0, 2.0;

    Poly1 poly(data(0), data(1));
    EXPECT(assert_equal(data, poly.coefficients(), 1e-8));
  }
//  {
//    Node node1;
//    node1 << 1.0, 0.0, 0.0;
//    Node node2;
//    node2 << 5.0, 0.0, 0.0;
//    Poly3 poly(node1, node2);

//    Vector4 expected;
//    expected << 0.0, 0.0, 0.0, 0.0;
//    EXPECT(assert_equal(expected, poly.coefficients(), 1e-8));
//  }
//  {
//    Node node1;
//    node1 << 1.0, 1.0, 1.0;
//    Node node2;
//    node2 << 5.0, 5.0, 1.0;
//    Poly3 poly(node1, node2);

//    Vector4 expected;
//    expected << 0.0, 1.0, 0.0, 0.0;
//    EXPECT(assert_equal(expected, poly.coefficients(), 1e-8));
//  }

  EXPECT(Poly1::dimension == 2);
}

/* ************************************************************************* */
static double evaluate_y(const Poly1& poly, const double& x) {
  return poly.y(x);
}

/* ************************************************************************* */
TEST(Poly1, y) {
  {
    Vector2 data;
    data << 1.0, 1.0;

    Poly1 poly(data);
    Matrix12 J_data_actual;
    Matrix11 J_x_actual;
    double x = 0, y_expected = 1.0;
    double y = poly.y(x, J_data_actual, J_x_actual);
    EXPECT_DOUBLES_EQUAL(y_expected, y, 1e-8);
    Matrix12 J_data_expected = numericalDerivative21(evaluate_y, poly, x);
    Matrix11 J_x_expected = numericalDerivative22(evaluate_y, poly, x);
    EXPECT(assert_equal(J_data_expected, J_data_actual, 1e-8));
    EXPECT(assert_equal(J_x_expected, J_x_actual, 1e-8));
  }
}

/* ************************************************************************* */
int main() { TestResult tr; return TestRegistry::runAllTests(tr);}
/* ************************************************************************* */
