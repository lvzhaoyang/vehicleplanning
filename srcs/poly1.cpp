#include "poly1.h"

#include <gtsam/base/Vector.h>

#include <Eigen/Dense>

using namespace gtsam;
using namespace std;

/* ************************************************************************* */
//Poly1::Poly1 (const Node &node1, const Node &node2) {
//  Matrix4 A;
//  Vector4 b;
//  double x1 = node1(0), x2 = node2(0);
//  A << 1, x1, x1*x1, x1*x1*x1,
//      1, x2, x2*x2, x2*x2*x2,
//      0, 1,  2*x1,  3*x1*x1,
//      0, 1,  2*x2,  3*x2*x2;
//  b << node1(1), node2(1), node1(2), node2(2);
//  data = A.colPivHouseholderQr().solve(b);
//}

/* ************************************************************************* */
void Poly1::print(const string &s) const {
    cout << s << "(" << data(0) << "," << data(1) << ")" << endl;
}

/* ************************************************************************* */
bool Poly1::equals(const Poly1 &poly1, double tol) const {
    return equal_with_abs_tol(data, poly1.data, tol);
}

/* ************************************************************************* */
Poly1 Poly1::retract(const Vector2 &v) const {
    return Poly1(data+v);
}

/* ************************************************************************* */
Vector2 Poly1::localCoordinates(const Poly1 &s) const {
    return s.data - data;
}

/* ************************************************************************* */
double Poly1::y(const double& x, gtsam::OptionalJacobian<1, 2> J_data,
                gtsam::OptionalJacobian<1, 1> J_x) const {
    double y = data(0) + data(1) * x;

    if (J_data) {
        *J_data << 1, x;
    }

    if (J_x) {
        *J_x << data(1);
    }

    return y;
}

/* ************************************************************************* */
//Poly1 Poly1::Create(const Node& node1, const Node& node2) {
//  return Poly1(node1, node2);
//}
