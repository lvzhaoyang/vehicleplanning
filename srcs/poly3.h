
#pragma once

#include <gtsam/base/Manifold.h>
#include <gtsam/base/Matrix.h>
#include <gtsam/nonlinear/Expression.h>

typedef gtsam::Vector3 Vector3;
typedef gtsam::Vector4 Vector4;

typedef Vector3 Node;

/** A polynomial 3
 */
class Poly3 {

private:
  Vector4 data; ///< the coefficents of the polynomial

public:

  enum {
    dimension = 4
  };

  /// @name constructors
  /// @{
  Poly3() {}

  Poly3(double a0, double a1, double a2, double a3) {
    data = Vector4(a0, a1, a2, a3);
  }

  explicit Poly3(const Vector4& poly)
    : data(poly) {
  }

  explicit Poly3(const Node& node1, const Node& node2);

  /// @}
  /// @name Testable
  /// @{

  /** print with optional string*/
  void print(const std::string& s="") const;

  bool equals(const Poly3& poly3, double tol=1e-9) const;

  /// @{
  /// @name Manifold
  /// @{

  /** retract function*/
  Poly3 retract(const Vector4& v) const;

  /** local coordinate function*/
  Vector4 localCoordinates(const Poly3& s) const;

  /** Dimensionality of the tangent space = 4DOF*/
  inline static size_t Dim() {
    return dimension;
  }

  /** Dimensionality of the tangent space = 4DOF*/
  inline size_t dim() const {
    return dimension;
  }

  /// @}
  /// @name evaluations
  /// @{

  /** return the coefficients of the data*/
  Vector4 coefficients() const {
    return data;
  }

  /** return the Y coordinate value, given a X
     * @param x
     * @param J_data Jacobian w.r.t. data
     * @param J_x Jacobian w.r.t. x
     */
  double y (const double& x,
            gtsam::OptionalJacobian<1,4> J_data = boost::none,
            gtsam::OptionalJacobian<1,1> J_x = boost::none) const;

  /** Create a Poly3 type given two nodes
     * @param node1
     * @param node2
     * @return
     */
  static Poly3 Create(const Node& node1, const Node& node2);
};

namespace gtsam {

/// Define the traits for gtsam related usage
template<> struct traits<Poly3> : public internal::Manifold<Poly3> {
};

template<> struct traits<const Poly3> : public internal::Manifold<Poly3> {
};

/// Define expression for gtsam

typedef Expression<Poly3> Poly3_;
typedef Expression<double> double_;

/// expression to evaluate y, given x
static double_ Evaluate_Y(const Poly3_& poly3, const double_& x) {
  return double_(poly3, &Poly3::y, x);
}

}


